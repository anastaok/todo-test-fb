import React from "react";

import "./AddTaskInputStyles.scss";

/**
 * @component компонент добавления задачи с её датой завершения
 * @param {PropType} props в компонент передаются props из TodoList, где хранятся чтение данных с fb, состояния input, даты и функция добавления задачи и даты
 * @returns форма c возможностью добавить заголовок задачи и дату её завершения
 */

const AddTodoInput = ({ addTask, inputTask, inputValue, date, setDate }) => {
  return (
    <form onSubmit={addTask} className="addСontainer">
      <input
        type="text"
        onChange={inputTask}
        value={inputValue}
        className="inputTask"
        placeholder="Введите задачу..."
      />
      <input
        type="date"
        className="inputDate"
        value={date}
        onChange={(e) => setDate(e.target.value)}
      />
      <div type="submit" onClick={addTask} className="addTask">
        Добавить задачу
      </div>
    </form>
  );
};

export default AddTodoInput;
