import React, { useEffect, useState } from "react";
import dayjs from "dayjs";
import {
  query,
  collection,
  onSnapshot,
  updateDoc,
  doc,
  addDoc,
  deleteDoc,
  orderBy,
  serverTimestamp,
} from "firebase/firestore";

import { db } from "../../firebase";
import TodoItem from "../TodoItem";
import AddTodoInput from "../AddTaskInput";
import "./TodoStyles.scss";

/**
 * @component компонент отрисовки списка добавленных задач
 */

const Todo = () => {
  /**
   * @param {object} объект-состояние всех полученных задач с firebase
   */
  const [todos, setTodos] = useState([]);

  /**
   * @param {object} объект-состояние изменяемых параметров задачи (заголовок)
   */
  const [inputValue, setInputValue] = useState("");

  /**
   * @param {object} объект-состояние дат завершения задач
   */
  const [date, setDate] = useState("");

  /**
   * @const currentDate получение текущей даты с использованием бибилиотеки dayjs, которая переводится в необходимый формат
   */
  const currentDate = dayjs().format("DD.MM.YYYY");

  /**
   * получение всех данных задач, полученных с firebase
   */
  useEffect(() => {
    const sortedTodos = query(collection(db, "todos"), orderBy("timestamp"));
    const unsubscribe = onSnapshot(sortedTodos, (snapshot) => {
      const allTodos = [];
      snapshot.forEach((doc) => {
        allTodos.push({ ...doc.data(), id: doc.id });
      });
      setTodos(allTodos);
    });
    return () => unsubscribe();
  }, []);

  /**
   * @function addTask функция добавления задачи в список задач и отправка их на firebase
   * @asynс
   */
  const addTask = async (e) => {
    e.preventDefault();
    if (!inputValue || !date) {
      alert(
        "Пожалуйста, введите заголовок задачи и дату её дату завершения :)"
      );
      return;
    }
    await addDoc(collection(db, "todos"), {
      title: inputValue,
      completed: false,
      description: "",
      file: "",
      date: date,
      timestamp: serverTimestamp(),
    });
    setDate("");
    setInputValue("");
  };

  /**
   * @function inputTask функция получения изменяемого input при вводе заголовка задачи
   */
  const inputTask = ({ target }) => {
    const inputValue = target.value;
    setInputValue(inputValue);
  };

  /**
   * @function checkTask функция переключения показателя выполнения задачи
   * @async
   */
  const checkTask = async (task) => {
    await updateDoc(doc(db, "todos", task.id), {
      completed: !task.completed,
    });
  };

  /**
   * @function deleteTask функция удаления задач
   * @typedef id уникальный ID token задачи, присваемый в fb
   * @async
   */

  const deleteTask = async (id) => {
    await deleteDoc(doc(db, "todos", id));
  };

  return (
    <div className="mainContainer">
      <div className="titleTodo">
        <div className="titleText">Список задач</div>
        <div className="dateToday">{currentDate}</div>
      </div>
      <AddTodoInput
        addTask={addTask}
        inputTask={inputTask}
        inputValue={inputValue}
        date={date}
        setDate={setDate}
      />
      <div className="tasks">
        {todos.length ? (
          todos.map((task) => {
            return (
              <React.Fragment key={task.id}>
                <TodoItem
                  task={task}
                  checkTask={checkTask}
                  deleteTask={deleteTask}
                />
              </React.Fragment>
            );
          })
        ) : (
          <div>Нет задач... Ты свободен(на)!</div>
        )}
      </div>
      <div>Число задач: {todos.length}</div>
    </div>
  );
};

export default Todo;
