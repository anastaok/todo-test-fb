import { Fragment } from "react";
import "./ModalStyles.scss";

/**
 * @component всплывающее модальное окно, которое позволяет редактировать данные одной задачи (заголовок, описание, дату завершения, удаление файла и его скачивание)
 * @param {PropType} props в компонент передаются props из TodoItem, где хранятся данные с firebase (объект task), состояние модального окна (open,setOpen), функции редактирования задач и удаления прикрепленного файла (handleChangeTask, handleEditTask, deleteFileTask), состояние изменившихся полей задачи (newValueTask)
 * @returns возвращает компонент модального окна, в котором реализуется закрытие модального окна, редактирование inputs заголовка, описания, даты и прикрепленного файла
 */

const Modal = ({
  task,
  open,
  setOpen,
  newValueTask,
  handleChangeTask,
  handleEditTask,
  deleteFileTask,
}) => {
  /**
   * @function clickSave функция, которая при клике сохраняет изменения в полях задачи и закрывает модальное окноcd
   */
  const clickSave = () => {
    handleEditTask();
    setOpen(false);
  };
  return (
    <div className={`overlay animated ${open ? "show" : ""}`}>
      <div className="modal">
        <span className="iconTaskModal" onClick={() => setOpen(!open)}>
          &#10008;
        </span>
        <div className="titleInputWrapper">
          <div className="titleTask">Задача:</div>
          <input
            className="titleInput"
            name="title"
            type="text"
            value={newValueTask.title}
            onChange={handleChangeTask}
          />
        </div>
        <div className="descTextWrapper">
          <div className="titleDesc">Описание:</div>
          <textarea
            className="descTextarea"
            name="description"
            type="text"
            value={newValueTask.description}
            onChange={handleChangeTask}
          />
        </div>
        <div className="titleInputWrapper">
          <div className="titleTask">Дата завершения:</div>
          <input
            type="date"
            name="date"
            className="dateInput"
            value={newValueTask.date}
            onChange={handleChangeTask}
          />
        </div>
        <div className="fileWrapper">
          {task.file && (
            <Fragment>
              <a
                className="fileLink"
                href={task.file}
                target="_blank"
                rel="noreferrer"
              >
                <button className="buttonModal">СКАЧАТЬ ФАЙЛ</button>
              </a>
              <button className="buttonModal" onClick={deleteFileTask}>
                УДАЛИТЬ ФАЙЛ
              </button>
            </Fragment>
          )}
        </div>
        <button className="buttonModal" onClick={clickSave}>
          Сохранить
        </button>
      </div>
    </div>
  );
};

export default Modal;
