import React, { useState } from "react";
import { updateDoc, doc } from "firebase/firestore";

import { db } from "../../firebase";
import TaskExecutionTime from "./components/TaskExecutionTime";
import TaskFile from "./components/TaskFile";
import Modal from "../Modal";
import "./TodoItemStyles.scss";

/**
 *
 * @component отрисовка одной задачи
 * @param {PropType} props в компонент передаются props из Todo где хранятся данные с firebase (объект task), состояние выполнения задачи (checkTask) и ее удаления(deleteTask)
 * @returns компонент задачи с заголовком, описанием, датой завершения, состоянием выполнения задачи и возможностью удаления, прикрепления файла и редактирования в модальном окне
 */

const TodoItem = ({ task, checkTask, deleteTask }) => {
  /**
   * @param {object} состояние модального окна (открыт/закрыт)
   */
  const [openModal, setOpenModal] = useState(false);

  /**
   * @param {object} состояние редактируемой задачи (полей заголовка, описания, даты завершения)
   */
  const [newValueTask, setNewValueTask] = useState({
    title: task.title,
    description: task.description,
    date: task.date,
  });

  /**
   * @function handleEditTask функция изменения данных задачи (полей заголовка, описания, даты завершения), которые отправляются нa firebase
   * @async
   */
  const handleEditTask = async () => {
    await updateDoc(doc(db, "todos", task.id), {
      title: newValueTask.title,
      description: newValueTask.description,
      date: newValueTask.date,
    });
  };

  /**
   * @function handleChangeTask функция редактирования данных задачи (полей заголовка, описания, даты завершения), которая изменяет состояние newValueTask за счет переписывания соответствующего поля в объекте состояния (определение поля за счет типа name)
   */
  const handleChangeTask = (event) => {
    setNewValueTask({
      ...newValueTask,
      [event.target.name]: event.target.value,
    });
  };

  /**
   * @function deleteFileTask функция удаления прикрепленного файла задачи по её id
   */
  const deleteFileTask = () => {
    updateDoc(doc(db, "todos", task.id), {
      file: "",
    });
  };

  return (
    <div className="taskCountainer" key={task.id}>
      <div className={`${task.completed ? "checkTask" : ""} taskItem`}>
        <input
          checked={task.completed}
          onChange={() => checkTask(task)}
          className="checkbox"
          type="checkbox"
        />
        <div className="taskInformation">
          <div className="taskTitle">{task.title}</div>
          {task.description ? (
            <li className="taskDescription"> {task.description}</li>
          ) : null}
          <TaskExecutionTime date={task.date} />
        </div>
        <Modal
          task={task}
          open={openModal}
          setOpen={setOpenModal}
          newValueTask={newValueTask}
          handleChangeTask={handleChangeTask}
          handleEditTask={handleEditTask}
          deleteFileTask={deleteFileTask}
        />
      </div>

      <div className="iconTaskWrapper">
        <TaskFile task={task} />
        <div onClick={() => setOpenModal(!openModal)} className="iconTask">
          &#10000;
        </div>
        <div className="iconTask" onClick={() => deleteTask(task.id)}>
          &#10008;
        </div>
      </div>
    </div>
  );
};

export default TodoItem;
